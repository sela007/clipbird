﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Clipbird
{
    public class TreeItem
    {
        public TreeItem()
        {
            Items = new List<TreeItem>();
        }


        public TreeItem (TreeNode node)
        {
            Items = new List<TreeItem>();
            Name = node.Text;
            Text = node.Text;
            Node = node;
            node.Tag = this;
        }


        public void AddItem(TreeNode node)
        {
            TreeItem itm = new TreeItem(node);
            node.Tag = itm;
        }


        public ItemType Type { get; set; }
        public string Name { get; set; }
        public string  Text { get; set; }
        public List<TreeItem> Items { get; set; }

        public bool Expanded { get; set; }
        [XmlIgnore]
        public TreeNode Node { get; set; }

    }

    public enum ItemType
    {
        Folder,
        Item
    }
}
