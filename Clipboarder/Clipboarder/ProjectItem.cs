﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Clipbird
{
    public class ProjectItem
    {
        public string  Description { get; set; }
        public string Text { get; set; }
        public bool IsTitle { get; set; }
        [XmlIgnore]
        public DataGridViewRow DataRow { get; set; }
    }
    
}
