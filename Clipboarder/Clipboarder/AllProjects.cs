﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clipbird
{
    public class AllProjects
    {

        public AllProjects()
        {
            Items = new List<Project>();
        }
        public List<Project> Items { get; set; }
        public string Owner { get; set; }

    }
}
