﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clipbird
{
    public class Project
    {
        public Project()
        {
            Items = new List<TreeItem>();

        }
        public string Name { get; set; }
        public List<TreeItem> Items { get; set; }
        public string Folder { get; set; }

    }
}
