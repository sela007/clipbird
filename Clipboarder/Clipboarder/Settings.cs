﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Clipbird
{
    public class Settings
    {
        public Settings()
        {
  cMonitor = new List<string>();
            ColumnsWidth = new int[3];
            ColumnsWidth[0] = 120;
            FormWidth = 300;
            FormHeight = 500;
            FormLeft = 100;
            FormTop = 100;
            TopMost = true;
            SplitterTop = 0;
            CMonitorMaximized = false;
            SendOnDblClick = false;
            FrmPrevWidth = 300;
            FrmPrevHeight = 300;
            FrmPrevTop = 100;
            FrmPrevLeft = 100;


        }
        public int FrmPrevWidth { get; set; }
        public int FrmPrevHeight{ get; set; }
        public int FrmPrevTop { get; set; }
        public int FrmPrevLeft { get; set; }


        public int[] ColumnsWidth { get; set; }
        public int FormWidth { get; set; }
        public int FormHeight { get; set; }
        public int FormTop { get; set; }
        public int FormLeft { get; set; }
        public bool TopMost { get; set; }

        public bool SendOnDblClick { get; set; }
        public int SplitterTop { get; set; }

        public string CurrentSaveFolder { get; set; }
        public bool CMonitorMaximized { get; set; }

        public List<string> cMonitor { get; set; }

        public static void SaveClass(object c, string filename, Type t)
        {
            XmlSerializer writer = new XmlSerializer(t);
            System.IO.StreamWriter file = new System.IO.StreamWriter(filename);
            writer.Serialize(file, c);
            file.Close();
            file.Dispose();
        }

        public static object LoadClass(string filename, Type t)
        {
            object o = new object();
            if (System.IO.File.Exists(filename))
            {
                System.Xml.Serialization.XmlSerializer reader = new
                System.Xml.Serialization.XmlSerializer(t);
                System.IO.StreamReader file = new System.IO.StreamReader(filename);
                o = reader.Deserialize(file);
                file.Dispose();
                return o;
            }
            return null;
        }
    }
}
