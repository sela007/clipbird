﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clipbird
{
    public partial class frmNew : Form
    {
        public frmNew(string label, string def="")
        {
            InitializeComponent();
            textBox1.Text = def;
            label1.Text = label;
            
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();

        }

        private void frmNew_Load(object sender, EventArgs e)
        {
            textBox1.SelectAll();

        }
    }
}
