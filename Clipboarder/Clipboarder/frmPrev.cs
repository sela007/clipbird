﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
 
namespace Clipbird
{
    public partial class frmPrev : Form
    {
         string currentSaveFolder;
        string tempDoc;
        string Filepath = "";
        Settings settings;
        ScintillaNET.Scintilla sc1;

        public frmPrev(string filepath,Settings sett,string saveFolder="")
        {
            InitializeComponent();

            settings = sett;

            Filepath = filepath;
            currentSaveFolder = saveFolder;

            cbLang.SelectedIndex = 1;

            
            if (System.IO.File.Exists(filepath))

            {
                this.Text = System.IO.Path.GetFileName(filepath);
                sc1.Text = System.IO.File.ReadAllText(filepath);
            }
            else
            {
                this.Text = filepath;
                sc1.Text = filepath;
            }
            tempDoc = sc1.Text;

            if(filepath.ToLower().EndsWith(".cs" ))cbLang.Text="c#";
            if(filepath.ToLower().EndsWith(".xml" ))cbLang.Text="xml";
            if(filepath.ToLower().EndsWith(".txt" ))cbLang.Text="text";
            if(filepath.ToLower().EndsWith(".java" ))cbLang.Text="java";
            if(filepath.ToLower().EndsWith(".cpp" ))cbLang.Text="c++";
            if(filepath.ToLower().EndsWith(".sql" ))cbLang.Text="sql";

            CanSave();
                    
        }

        private void frmPrev_Load(object sender, EventArgs e)
        {



            this.Controls.Add(sc1);
         
            Height = settings.FrmPrevHeight;
            Top = settings.FrmPrevTop;
            Left = settings.FrmPrevLeft;
            Width = settings.FrmPrevWidth;

            sc1.Dock = DockStyle.Fill;
            this.sc1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            sc1.Top = toolStrip1.Top + toolStrip1.Height;
            sc1.Width = this.Width-20;
            sc1.Height = this.Height - 100;
            sc1.Margins[0].Width = 25;
            //sc1.Margins[0].Width = 20;


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();

        }

        private void btnCopyAll_Click(object sender, EventArgs e)
        {
             
            sc1.Clipboard.Copy(0, sc1.Text.Length);

 

        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            if (SaveAs()) Close();

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            if(SaveAs(btn.Text))
            {
                Close();
            }

        }

        private void cbLang_SelectedIndexChanged(object sender, EventArgs e)
        {
  
            switch (cbLang.SelectedIndex)
            {
                case 0:
                    sc1.ConfigurationManager.Language = "text";
                    break;

                case 1:
                    sc1.ConfigurationManager.Language = "cs";
                    break;
                case 2:
                    sc1.ConfigurationManager.Language = "xml";
                    break;
                case 3:
                    sc1.ConfigurationManager.Language = "mssql";
                    break;
                case 4:
                    sc1.ConfigurationManager.Language = "cpp";
                    break;
                case 5:
                    sc1.ConfigurationManager.Language = "java";
                    break;
            }

            sc1.ConfigurationManager.Configure();


        }

        private void btnPaste_Click(object sender, EventArgs e)
        {sc1.UndoRedo.BeginUndoAction();
            sc1.Clipboard.Paste();
            sc1.UndoRedo.EndUndoAction();


        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            sc1.Clipboard.Copy();

        }

        private void btnCut_Click(object sender, EventArgs e)
        {
            sc1.Clipboard.Cut();
            sc1.UndoRedo.BeginUndoAction();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
          if(sc1.UndoRedo.CanUndo)
            {
                sc1.UndoRedo.Undo();

            }
        }

        private void btnRedo_Click(object sender, EventArgs e)
        {
          if(sc1.UndoRedo.CanRedo)
            {
                sc1.UndoRedo.Redo();

            }
        }


        int MatchPosition = 1;
        int MatchCount = 0;
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

            MatchPosition = 1;
            MatchCount  = SearchCount(txtSearch.Text);
            if (MatchCount > 0)
            {
                lblSearchCount.Enabled = true;
                lblSearchCount.Text = "Match " + MatchPosition.ToString() + "/" + MatchCount.ToString();
            }
            else {
                lblSearchCount.Enabled = false;

                lblSearchCount.Text = "";
            }
             
            Search(txtSearch.Text);
        }

         
        private bool Search(string text)
        {

            if (text.Length == 0)
            {
                sc1.Selection.Length = 0;


                return false;
            }
            // Indicators 0-7 could be in use by a lexer
            // so we'll use indicator 8 to highlight words.
            try
            {

                if (btnMatchCase.Checked)
                {
                    MatchCount = sc1.FindReplace.FindAll(text,ScintillaNET.SearchFlags.MatchCase).Count() ;
                }else MatchCount = sc1.FindReplace.FindAll(text).Count();

               

                if (MatchCount == 0)
                {
                    sc1.Selection.SelectNone();
                    MatchPosition = 0;
                    return false;
                }
                else {
                    sc1.FindReplace.Find(text).Select();return true;
                }

                

            }
            catch { }
            return false;

        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Return)
                NextSearch(txtSearch.Text);
            if (e.KeyCode==Keys.Return)
                e.SuppressKeyPress = true;
            
        }
        private void NextSearch(string text)
        {

            sc1.FindReplace.FindNext(text).Select();
        
            MatchPosition++;
            if (MatchPosition > MatchCount) MatchPosition = 1;

          
            lblSearchCount.Text = "Match " + MatchPosition.ToString() + "/" + MatchCount.ToString();


        }
        private int SearchCount(string text)
        {

            if (text == "") return 0;
            int sIndex = 0;
            int count = 0;

            while (sIndex > -1)
            {
                 
                if (btnMatchCase.Checked)
                {
                    sIndex = sc1.Text.IndexOf(text, sIndex);
                }
                else sIndex = sc1.Text.IndexOf(text, sIndex, StringComparison.CurrentCultureIgnoreCase);

                if (sIndex > -1) count++;
                if(sIndex !=-1)  sIndex++;
            }
            
            return count;
            
        }

        private void sc1_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
             
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void txtSearch_MouseClick(object sender, MouseEventArgs e)
        {
            

        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
      
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            

        }

        private void btnMatchCase_Click(object sender, EventArgs e)
        {
            btnMatchCase.Checked = !btnMatchCase.Checked;


        }

        private void txtSearch_Click(object sender, EventArgs e)
        {
            txtSearch.SelectAll();
        }

        private void sc1_TextChanged(object sender, EventArgs e)
        {
            CanSave();
        }


      

        private bool CanSave()
        {
            if (sc1.Text != tempDoc)
            {
                btnSaveDoc.Image = im1.Images[1];
                return true;

            }
            else {
                btnSaveDoc.Image = im1.Images[0];
                return false;
            }
        }

        private void btnSaveDoc_ButtonClick(object sender, EventArgs e)
        {
            if (CanSave())
            {
                Save();
            }
        }

        private void Save()
        {
            if (System.IO.File.Exists(Filepath))
            {
                try
                {
                    System.IO.File.WriteAllText(Filepath, sc1.Text);
                    btnSaveDoc.Image = im1.Images[0];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else SaveAs();
        }

        private bool SaveAs(string filter = "*.*")
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.Filter = @"Text|*.txt|Xml|*.xml|C# |*.cs|C++|*.cpp|Sql|*.sql|Java|*.java";
            if (cbLang.Text == "text") fd.FilterIndex = 1;
            if (cbLang.Text == "xml") fd.FilterIndex = 2;
            if (cbLang.Text == "c#") fd.FilterIndex = 3;
            if (cbLang.Text == "cpp") fd.FilterIndex = 4;
            if (cbLang.Text == "sql") fd.FilterIndex = 5;
            if (cbLang.Text == "java") fd.FilterIndex = 6;

            if (System.IO.Directory.Exists(currentSaveFolder))
                fd.InitialDirectory = currentSaveFolder;

            switch (filter)
            {
                case "SQL":
                    fd.FilterIndex = 5;
                    break;
                case "CS":
                    fd.FilterIndex = 3;
                    break;
                case "TXT":
                    fd.FilterIndex = 1;
                    break;
            }

            if (fd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    System.IO.File.WriteAllText(fd.FileName, sc1.Text);
                    currentSaveFolder = System.IO.Path.GetDirectoryName(fd.FileName);
                    this.Text = System.IO.Path.GetFileName(fd.FileName);
                    Filepath = fd.FileName;
                    tempDoc = sc1.Text;
                    CanSave();
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "");
                    return false;
                }


            }
            return false;

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            tempDoc = "";
            sc1.Text = "";
            Filepath = "";
            this.Text = "";


        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs();

        }

        private void frmPrev_FormClosing(object sender, FormClosingEventArgs e)
        {
            settings.FrmPrevHeight = this.Height;
            settings.FrmPrevLeft = this.Left;
            settings.FrmPrevTop = this.Top;
            settings.FrmPrevWidth = this.Width;

        }

        private void btnSearch_ButtonClick(object sender, EventArgs e)
        {
            NextSearch(txtSearch.Text);
        }

        private void cbLang_Click(object sender, EventArgs e)
        {

        }
    }
}
