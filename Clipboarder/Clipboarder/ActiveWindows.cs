﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Clipbird
{
    public class ActiveWindows
    {
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);
        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        private static extern IntPtr GetParent(IntPtr hWnd);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        enum GetWindow_Cmd : uint { FIRST = 0, LAST = 1, PREVIOUS = 2, NEXT = 3, OWNER = 4, CHILD = 5, ENABLEDPOPUP = 6 } // Relative code status of window

        /*
 * Change status of the window
 */
        private void changeWindow(uint status)
        {
            IntPtr targetProcess = GetWindow(Process.GetCurrentProcess().MainWindowHandle, status); // Get target process with status relative to the main window
            while (true) // Loop for each processes
            {
                IntPtr parentProces = GetParent(targetProcess); // Get parent process of the target process
                if (parentProces.Equals(IntPtr.Zero)) // No partent process of the target process
                    break; // Target process is the last one, it has the desired status
                targetProcess = parentProces; // Parent process of the current target process becomes the new target process
            }
            SetForegroundWindow(targetProcess); // Set target process to the foreground
        }

        public void PreviousWindow()
        {
            changeWindow((uint)GetWindow_Cmd.PREVIOUS); // Put 'NEXT' window to the foreground
        }

    }
}
