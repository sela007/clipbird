﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clipbird
{
    public partial class Form1 : Form
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public Form1()
        {
            InitializeComponent();
        }

        private void dg1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tab1_SelectedIndexChanged(object sender, EventArgs e)
        {

            var tab = (ucProject)tab1.SelectedTab.Controls[0];
             
           
        }

        ClipboardMonitor cm;
        public Settings settings;
        public AllProjects Projects;
        public string ProjectsFilePath = "projects.xml";
        private void Form1_Load(object sender, EventArgs e)
        {
            ClipboardMonitor cc = new ClipboardMonitor();

            //MessageBox.Show( cc!=null ? cc.Size.ToString(): "null");


            settings = new Settings();
            int cw = 120;
            if (System.IO.File.Exists("settings.xml"))
            {

                settings =(Settings) Settings.LoadClass("settings.xml", typeof(Settings));

                TopMost = settings.TopMost;
                Width = settings.FormWidth;
                Height = settings.FormHeight;
                Left = settings.FormLeft;
                Top = settings.FormTop;
                splitContainer1.SplitterDistance = settings.SplitterTop;
                //splitContainer1.Panel1Collapsed = settings.CMonitorMaximized;
                cw = settings.ColumnsWidth[0];
                foreach(string s in settings.cMonitor)
                {
                    dgMonitor.Rows.Add(s, "c");
                }


                
            }


            Projects = new AllProjects();
            Projects =(AllProjects) Settings.LoadClass(ProjectsFilePath, typeof(AllProjects));
            if (Projects == null) Projects = new AllProjects();
            
            
            LoadProjects(Projects);

            this.TopMost = true;      
   
        }

        private void LoadProjects(AllProjects projects)
        {
            cm = new ClipboardMonitor();
            cm.ClipboardChanged += Cm_ClipboardChanged;  
                    
            foreach(var p in projects.Items)
            {
                
                CreateProjectTab(p);
            }

        }

        private void Cm_ClipboardChanged(object sender, ClipboardChangedEventArgs e)
        {
            if (notNow)
            {

                notNow = false;
                notNow2 = true;
                return;
            }
            if (notNow2)
            {
                notNow2 = false;return;
            }

            foreach (DataGridViewRow dr in dgMonitor.SelectedRows) dr.Selected = false;


            
            string s = e.DataObject.GetData(DataFormats.Text).ToString();
            if (dgMonitor.Rows.Count > 0)
            {
                if(s!=dgMonitor.Rows[0].Cells[0].Value.ToString())
                dgMonitor.Rows.Insert(0,s, "c");
                dgMonitor.Rows[0].Selected = false;
                 

            }
            else
            {
                
                dgMonitor.Rows.Insert(0,s, "Copy");
                dgMonitor.Rows[0].Selected =false;
            }

            if (dgMonitor.Rows.Count > 1000) dgMonitor.Rows.RemoveAt(1000);
           

        }

        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {

            bool b = this.TopMost;
            this.TopMost = false;
            frmNew frm = new frmNew("Project name");
         
            if (frm.ShowDialog() == DialogResult.OK)
            {
                Project p = new Project();
                p.Name = frm.textBox1.Text;
                Projects.Items.Add(p);
                CreateProjectTab(p);
            }
            this.TopMost = b;

        }

        private void CreateProjectTab(Project p)
        {

            TabPage tp = new TabPage(p.Name);
 
            ucProject ucp = new ucProject(p,this);
            ucp.DataChange += Ucp_DataChange;
            ucp.CopyPressed += Ucp_CopyPressed;
            ucp.LoadTree();
            ucp.Dock = DockStyle.Fill;
            tp.Controls.Add(ucp);
            tab1.TabPages.Add(tp);

        }

        private void Ucp_CopyPressed(object sender, CopyPressEvent e)
        {
            Put(e.Data);

        }

        private void Put(string s)
        {
            Clipboard.SetText(s);
            ActiveWindows aw = new ActiveWindows();
            //aw.PreviousWindow();
            this.WindowState = FormWindowState.Minimized;
            SendKeys.Send("^v");
            System.Threading.Thread.Sleep(20);

            this.WindowState = FormWindowState.Normal;
        }

 

        private void Ucp_DataChange(object sender, EventArgs e)
        {
            SaveProjects();
        }

        private void SaveProjects()
        {
            Settings.SaveClass(Projects, ProjectsFilePath, typeof(AllProjects));
        }

     

        private void 
            nitor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnClipboardHistory_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1Collapsed = !splitContainer1.Panel1Collapsed;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveProjects();

            settings.TopMost = this.TopMost;
            settings.FormWidth = Width;
            settings.FormHeight = Height;
            settings.FormLeft = Left;
            settings.FormTop = Top;
            settings.CMonitorMaximized = splitContainer1.Panel1Collapsed;
            settings.SplitterTop = splitContainer1.SplitterDistance;

            settings.cMonitor.Clear();
            foreach (DataGridViewRow dr in dgMonitor.Rows)
            {
                
                settings.cMonitor.Add(dr.Cells[0].Value.ToString());
            }

            Settings.SaveClass(settings, "settings.xml", typeof(Settings));

            cm.Dispose();

        }

        private void dgMonitor_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
          
        }

        private void removeProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Delete project?","",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Projects.Items.RemoveAt(tab1.SelectedIndex);
                tab1.TabPages.RemoveAt(tab1.SelectedIndex);
                SaveProjects();

            }
        }

        private void dgMonitor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgMonitor.Rows[e.RowIndex].Cells[0].Value == null) return;
            string str = dgMonitor.Rows[e.RowIndex].Cells[0].Value.ToString();

            //System.IO.File.WriteAllText("temp.txt", dgMonitor.Rows[e.RowIndex].Cells[0].Value.ToString());
            //string str = System.IO.File.ReadAllText("temp.txt");
            frmPrev frm = new frmPrev(str,settings, Projects.Items[tab1.SelectedIndex].Folder);
            frm.Show();

        }

        private void renameProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {

            bool b = this.TopMost;
            this.TopMost = false;
            frmNew frm = new frmNew(Projects.Items[tab1.SelectedIndex].Name);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (frm.textBox1.Text.Trim() == "") return;
                Projects.Items[tab1.SelectedIndex].Name = frm.textBox1.Text;
                tab1.TabPages[tab1.SelectedIndex].Text = frm.textBox1.Text;
                SaveProjects();
            }
            this.TopMost = b;
        }

        //kada se klikne u dgMonitor da se ne dodaje u isti
        bool notNow;
        bool notNow2;
        private void dgMonitor_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 4)
            {
                notNow = true;
                if (dgMonitor.Rows[e.RowIndex].Cells[0].Value != null)
                    Clipboard.SetText(dgMonitor.Rows[e.RowIndex].Cells[0].Value.ToString());

            }
            else if (e.ColumnIndex == 1)
            {
                

                notNow = true;
                if (dgMonitor.Rows[e.RowIndex].Cells[0].Value != null)
                    Put(dgMonitor.Rows[e.RowIndex].Cells[0].Value.ToString());
                
            }


            else
            {

 
            }
        }

        private void selectProjectFolderToolStripMenuItem_Click(object sender, EventArgs e)
        { FolderBrowserDialog fb = new FolderBrowserDialog();
            if (System.IO.Directory.Exists(Projects.Items[tab1.SelectedIndex].Folder ))
            {
                fb.SelectedPath = Projects.Items[tab1.SelectedIndex].Folder;
                if (fb.ShowDialog() == DialogResult.OK)
                {
                    Projects.Items[tab1.SelectedIndex].Folder = fb.SelectedPath;

                }

            }
            else
            {
               
                if (fb.ShowDialog() == DialogResult.OK)
                {
                    Projects.Items[tab1.SelectedIndex].Folder = fb.SelectedPath;
                }
            }

            SaveProjects();

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {



        }

        private void Search()
        {

            foreach (DataGridViewRow dr in dgMonitor.Rows)
            {

                if (!dr.Cells[0].Value.ToString().Contains(txtSearch.Text))
                {
                    dr.Visible = false;

                }
                else
                    dr.Visible = true;

            }
        }

        private void btnOib_Click(object sender, EventArgs e)
        {
            this.Text = GetOib();
            Put(this.Text);

        }

        private string GetOib()
        {
            string str = "";

            Random rnd = new Random(DateTime.Now.Minute * DateTime.Now.Second * DateTime.Now.Millisecond);

            List<int> nums = new List<int>();
            for(int i = 1; i <= 10; i++)
            {
                nums.Add(rnd.Next(0, 9));
            }

            int ost = 10;
            for(int i=0;i<=9;i++)
            {
                int curr = nums[i] + ost;
                int mod = curr % 10;
                if (mod == 0) mod = 10;
                int umno = mod * 2;
                ost = umno % 11;
            }

            foreach(int n in nums)
            {
                str += n.ToString();

            }
            ost = 11 - ost;
            if (ost == 10) ost = 0;
            str +=ost.ToString();
            
            return str;

        }

        private void btnOib_MouseMove(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Left)
            //{
            //    string str = GetOib();
            //    btnOib.DoDragDrop(str, DragDropEffects.Copy);

            //}
        }

        private void dgMonitor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Move(object sender, EventArgs e)
        {

        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Minimized)
            {
                if(this.Left + this.Width > Screen.PrimaryScreen.WorkingArea.Width)
                {
                    this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
                }

                this.Top = 0;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();

        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;


        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Search();

        }
    }


}
