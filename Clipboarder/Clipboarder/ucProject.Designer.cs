﻿namespace Clipbird
{
    partial class ucProject
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucProject));
            this.strip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.browseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFolder = new System.Windows.Forms.Button();
            this.btnNewFile = new System.Windows.Forms.Button();
            this.csFiles = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.bcvbvcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bcvbvcbvcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Tv1 = new System.Windows.Forms.TreeView();
            this.IM1 = new System.Windows.Forms.ImageList(this.components);
            this.btnNewItem = new System.Windows.Forms.Button();
            this.lblProj = new System.Windows.Forms.Label();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.strip.SuspendLayout();
            this.csFiles.SuspendLayout();
            this.SuspendLayout();
            // 
            // strip
            // 
            this.strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addFolderToolStripMenuItem,
            this.addItemToolStripMenuItem,
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripMenuItem1,
            this.browseToolStripMenuItem,
            this.toolStripMenuItem2,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem});
            this.strip.Name = "strip";
            this.strip.Size = new System.Drawing.Size(153, 192);
            // 
            // addFolderToolStripMenuItem
            // 
            this.addFolderToolStripMenuItem.Name = "addFolderToolStripMenuItem";
            this.addFolderToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.addFolderToolStripMenuItem.Text = "Add folder";
            this.addFolderToolStripMenuItem.Click += new System.EventHandler(this.addFolderToolStripMenuItem_Click);
            // 
            // addItemToolStripMenuItem
            // 
            this.addItemToolStripMenuItem.Name = "addItemToolStripMenuItem";
            this.addItemToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.addItemToolStripMenuItem.Text = "Add item";
            this.addItemToolStripMenuItem.Click += new System.EventHandler(this.addItemToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(127, 6);
            // 
            // browseToolStripMenuItem
            // 
            this.browseToolStripMenuItem.Name = "browseToolStripMenuItem";
            this.browseToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.browseToolStripMenuItem.Text = "Browse";
            this.browseToolStripMenuItem.Click += new System.EventHandler(this.browseToolStripMenuItem_Click);
            // 
            // btnFolder
            // 
            this.btnFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFolder.FlatAppearance.BorderSize = 0;
            this.btnFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFolder.Image = ((System.Drawing.Image)(resources.GetObject("btnFolder.Image")));
            this.btnFolder.Location = new System.Drawing.Point(280, 3);
            this.btnFolder.Name = "btnFolder";
            this.btnFolder.Size = new System.Drawing.Size(27, 30);
            this.btnFolder.TabIndex = 1;
            this.btnFolder.UseVisualStyleBackColor = true;
            this.btnFolder.Click += new System.EventHandler(this.btnFolder_Click);
            // 
            // btnNewFile
            // 
            this.btnNewFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewFile.FlatAppearance.BorderSize = 0;
            this.btnNewFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewFile.Image = ((System.Drawing.Image)(resources.GetObject("btnNewFile.Image")));
            this.btnNewFile.Location = new System.Drawing.Point(245, 3);
            this.btnNewFile.Name = "btnNewFile";
            this.btnNewFile.Size = new System.Drawing.Size(29, 30);
            this.btnNewFile.TabIndex = 2;
            this.btnNewFile.UseVisualStyleBackColor = true;
            this.btnNewFile.Click += new System.EventHandler(this.btnNewFile_Click);
            // 
            // csFiles
            // 
            this.csFiles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bcvbvcToolStripMenuItem});
            this.csFiles.Name = "csFiles";
            this.csFiles.Size = new System.Drawing.Size(113, 26);
            this.csFiles.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.csFiles_ItemClicked);
            this.csFiles.Click += new System.EventHandler(this.csFiles_Click);
            // 
            // bcvbvcToolStripMenuItem
            // 
            this.bcvbvcToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bcvbvcbvcToolStripMenuItem});
            this.bcvbvcToolStripMenuItem.Name = "bcvbvcToolStripMenuItem";
            this.bcvbvcToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.bcvbvcToolStripMenuItem.Text = "bcvbvc";
            // 
            // bcvbvcbvcToolStripMenuItem
            // 
            this.bcvbvcbvcToolStripMenuItem.Name = "bcvbvcbvcToolStripMenuItem";
            this.bcvbvcbvcToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.bcvbvcbvcToolStripMenuItem.Text = "bcvbvcbvc";
            // 
            // Tv1
            // 
            this.Tv1.AllowDrop = true;
            this.Tv1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Tv1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Tv1.FullRowSelect = true;
            this.Tv1.ImageIndex = 0;
            this.Tv1.ImageList = this.IM1;
            this.Tv1.Indent = 12;
            this.Tv1.ItemHeight = 20;
            this.Tv1.Location = new System.Drawing.Point(3, 39);
            this.Tv1.Name = "Tv1";
            this.Tv1.SelectedImageIndex = 0;
            this.Tv1.Size = new System.Drawing.Size(316, 350);
            this.Tv1.TabIndex = 3;
            this.Tv1.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.Tv1_AfterCollapse);
            this.Tv1.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.Tv1_AfterExpand);
            this.Tv1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.Tv1_AfterSelect);
            this.Tv1.DragDrop += new System.Windows.Forms.DragEventHandler(this.Tv1_DragDrop);
            this.Tv1.DragEnter += new System.Windows.Forms.DragEventHandler(this.Tv1_DragEnter);
            this.Tv1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Tv1_MouseDoubleClick);
            this.Tv1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Tv1_MouseDown);
            this.Tv1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Tv1_MouseMove);
            // 
            // IM1
            // 
            this.IM1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IM1.ImageStream")));
            this.IM1.TransparentColor = System.Drawing.Color.Transparent;
            this.IM1.Images.SetKeyName(0, "folder.png");
            this.IM1.Images.SetKeyName(1, "copy.png");
            // 
            // btnNewItem
            // 
            this.btnNewItem.FlatAppearance.BorderSize = 0;
            this.btnNewItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewItem.Image = ((System.Drawing.Image)(resources.GetObject("btnNewItem.Image")));
            this.btnNewItem.Location = new System.Drawing.Point(3, 3);
            this.btnNewItem.Name = "btnNewItem";
            this.btnNewItem.Size = new System.Drawing.Size(29, 30);
            this.btnNewItem.TabIndex = 2;
            this.btnNewItem.UseVisualStyleBackColor = true;
            this.btnNewItem.Click += new System.EventHandler(this.btnNewItem_Click);
            // 
            // lblProj
            // 
            this.lblProj.AutoSize = true;
            this.lblProj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblProj.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblProj.Location = new System.Drawing.Point(38, 9);
            this.lblProj.Name = "lblProj";
            this.lblProj.Size = new System.Drawing.Size(45, 20);
            this.lblProj.TabIndex = 4;
            this.lblProj.Text = "HAK";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(127, 6);
            // 
            // ucProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblProj);
            this.Controls.Add(this.Tv1);
            this.Controls.Add(this.btnNewItem);
            this.Controls.Add(this.btnNewFile);
            this.Controls.Add(this.btnFolder);
            this.Name = "ucProject";
            this.Size = new System.Drawing.Size(322, 392);
            this.Load += new System.EventHandler(this.ucProject_Load);
            this.strip.ResumeLayout(false);
            this.csFiles.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip strip;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Button btnFolder;
        private System.Windows.Forms.Button btnNewFile;
        private System.Windows.Forms.ContextMenuStrip csFiles;
        private System.Windows.Forms.ToolStripMenuItem bcvbvcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bcvbvcbvcToolStripMenuItem;
        private System.Windows.Forms.TreeView Tv1;
        private System.Windows.Forms.ImageList IM1;
        private System.Windows.Forms.Button btnNewItem;
        private System.Windows.Forms.ToolStripMenuItem addFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem browseToolStripMenuItem;
        private System.Windows.Forms.Label lblProj;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
    }
}
