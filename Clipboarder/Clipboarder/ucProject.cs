﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using Clipbird.Properties;

namespace Clipbird
{
    public partial class ucProject : UserControl
    {
        public ucProject(Project p,Form1 frm)
        {
            InitializeComponent();
            project = p;
            settings = frm.settings;
            form1 = frm;
            TitleBC = this.BackColor;
            lblProj.Text = p.Name;

        }


        Form1 form1;
        Settings settings;
        public delegate void delSettingsChanged(ucProject ucp);
        public delSettingsChanged SettingsChanged;

        public delegate void delDataChange(object sender,EventArgs e);
        public delegate void delCopyPress(object sender, CopyPressEvent e);
        public event delDataChange DataChange;
        public event delCopyPress CopyPressed;

        private Color ItmBC = Color.FromArgb(250,250,250);
        private Color ItmFC = Color.Black;
        private Color ItmFC2 = Color.Black;
        private Color TitleBC = Color.FromArgb(150,150,150);
        private Color TitleFC = Color.FromArgb(240, 240,240);


        public void AddItem(string text, TreeItem DestItem = null,ItemType type=ItemType.Item)
        {
            TreeNode node = new TreeNode();
            node.Text = text;

            TreeItem itm = new TreeItem(node);
            itm.Type = type;

            if (DestItem != null)
            {
                if (DestItem.Type == ItemType.Folder)
                {
                        DestItem.Items.Add(itm);
                        DestItem.Node.Nodes.Add(itm.Node);
                        SetColors(DestItem.Node);
                }
                else
                {
                    if (DestItem.Node.Parent != null)
                    {
                        TreeItem ParentItem = (TreeItem)DestItem.Node.Parent.Tag;
                        ParentItem.Items.Add(itm);
                        ParentItem.Node.Nodes.Add(itm.Node);
                        SetColors(DestItem.Node);
                    }
                }

            }
            else
            {
                project.Items.Add(itm);
                Tv1.Nodes.Add(itm.Node);
            }

            SelectedItem = itm;
            SetColors(itm.Node);

        }


        private void RaiseDataChange()
        {

            if (DataChange != null) DataChange(this, new EventArgs());
        }

        private Project project;

         
        private void dg_NewRowNeeded(object sender, DataGridViewRowEventArgs e)
        {

        }

         
        private void dg_SelectionChanged(object sender, EventArgs e)
        {

            //if (dg.SelectedRows != null && dg.SelectedRows.Count>0)
            //{
            //    if (dg.SelectedRows[0].Tag != null)
            //    {
            //        SelectedItem = (ProjectItem)dg.SelectedRows[0].Tag;
            //        SelectedItem.DataRow = dg.SelectedRows[0];
            //    }
            //    else { SelectedItem = null; }
            //}

        }

        private void ucProject_Load(object sender, EventArgs e)
        {
             

        }

        private void dg_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dg_CellEnter(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dg_RowHeadersWidthChanged(object sender, EventArgs e)
        {
           
        }

        private void dg_ColumnDividerWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {

        }

        private void dg_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
           
            
            SettingsChanged(this);
        }

        private void dg_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

     


        private void SetSelected(DataGridViewRow dr)
        {
     
            
        }
        private void SetSelected(ProjectItem item)
        {
 
        }



        private void dg_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1) return;
            
          
        }

        private void insertToolStripMenuItem_Click(object sender, EventArgs e)
        {


        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
       
            if (SelectedItem != null)
            {
                RemoveItem(SelectedItem);
                RaiseDataChange();
            }
             
        }



        private void dg_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void dg_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //if (e.RowIndex == dg.Rows.Count - 1)
            //{
            //    SelectedItem = new ProjectItem { Description = "", Text = "" };
            //    project.Items.Add(SelectedItem);
            //    SelectedIndex = e.RowIndex;
            //}
        }


         
        private void LoopDir(ToolStripMenuItem itm)
        {
            string path = itm.Tag.ToString();
            try {
                string[] dirs = System.IO.Directory.GetDirectories(path);
                foreach (string f in dirs)
                {
                    ToolStripMenuItem tDir = (ToolStripMenuItem)itm.DropDownItems.Add(System.IO.Path.GetFileName(f), Resources.folder);
                    tDir.Tag = f;
                    tDir.Image = Resources.folder;
                    tDir.Click += TDir_Click;

                    itm.DropDownItems.Add(tDir);
                    LoopDir(tDir);
                }

                string[] files = System.IO.Directory.GetFiles(itm.Tag.ToString());
                foreach (string f in files)
                {
                    ToolStripMenuItem i = (ToolStripMenuItem)itm.DropDownItems.Add(System.IO.Path.GetFileName(f), IconReader.GetFileIcon(f, IconReader.IconSize.Small, false).ToBitmap());
                    i.Tag = f;
                    i.Click += TDir_Click;

                }
            }
            catch (UnauthorizedAccessException) { }

        }

 

        private void btnFolder_Click(object sender, EventArgs e)
        {
            if (System.IO.Directory.Exists(project.Folder))
            {
                //System.Diagnostics.Process.Start("explorer.exe", project.Folder);
            }
            else
            {
                FolderBrowserDialog fb = new FolderBrowserDialog();
                if (fb.ShowDialog() == DialogResult.OK)
                {
                    project.Folder = fb.SelectedPath;
                    //System.Diagnostics.Process.Start("explorer.exe", fb.SelectedPath);
                }
                else return;

            }

            string[] files = System.IO.Directory.GetFiles(project.Folder);
            string[] dirs = System.IO.Directory.GetDirectories (project.Folder);
            csFiles.Items.Clear();
            ToolStripItem ti1= csFiles.Items.Add("Open folder");
            ToolStripItem ti2= csFiles.Items.Add("Change project folder");
            ti1.Tag = "OPEN";
            ti2.Tag = "CHANGE";
            csFiles.Items.Add("-");
 
            foreach(string f in dirs)
            {
                ToolStripMenuItem tDir = (ToolStripMenuItem)csFiles.Items.Add(System.IO.Path.GetFileName(f) ,Resources.folder);
                tDir.Tag = f;
                tDir.Click += TDir_Click; 
                LoopDir(tDir);

            }
            foreach (string f in files)
            {
              ToolStripItem i= csFiles.Items.Add(System.IO.Path.GetFileName(f),IconReader.GetFileIcon(f, IconReader.IconSize.Small ,false).ToBitmap());
                i.Tag = f;
            }

            csFiles.Show(Cursor.Position);
            return;

        }



        private void btnPaste_Click(object sender, EventArgs e)
        {
            //Paste();

            frmNew frm = new frmNew("New item");
            if (frm.ShowDialog() == DialogResult.OK)
            {
                //AddItem(frm.textBox1.Text);
            }

        }

        public void Paste()
        {
            if (Clipboard.ContainsText())
            {
                //AddItem(Clipboard.GetText(), "");
                RaiseDataChange();
           
            }
        }

        private void dg_CellLeave(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dg_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            var sd = e.StateChanged;

        }

        private void ChangeFolder()
        {
            FolderBrowserDialog fb = new FolderBrowserDialog();
            if (fb.ShowDialog() == DialogResult.OK)
            {
                project.Folder = fb.SelectedPath;
            }

        }

        private void btnNewFile_Click(object sender, EventArgs e)
        {
            frmPrev frm = new frmPrev("",settings, project.Folder);
            frm.Show();

        }
        private void TDir_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem i = (ToolStripMenuItem)sender;
            if(System.IO.File.Exists(i.Tag.ToString()))
            Run(i.Tag.ToString());


        }
        private void csFiles_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Tag == null) return;

            string tag = e.ClickedItem.Tag.ToString();

            switch (tag)
            {
                case "OPEN":
                        Run(project.Folder);
                    break;

                case "CHANGE":
                    ChangeFolder();
                    break;

                default:
                  if(  !System.IO.Directory.Exists(tag))
                        Run(e.ClickedItem.Tag.ToString());
                    break;
            }

        }

        private void UcProject_Click(object sender, EventArgs e)
        {
            
        }

        private void csFiles_Click(object sender, EventArgs e)
        {

        }
        private void Run(string path)
        {

            if (System.IO.Directory.Exists(path))
            {
                System.Diagnostics.Process.Start("explorer.exe", project.Folder);
            }else
            {
                if (System.IO.File.Exists(path))
                {
                    string ext1 = System.IO.Path.GetExtension(path);
                    if (ext1 == ".cs" || ext1==".cpp" || ext1 ==".sql" || ext1 == ".xml" || ext1 == ".java" || ext1 == ".txt")
                    {
                        OpenFile(path);
                    }
               
                    else
                    System.Diagnostics.Process.Start(path);
                }
            }    
        }

        private void OpenFile(string path)
        {
            
            frmPrev frm = new frmPrev(path,settings, project.Folder);
            frm.Show();
            
        }
        
        public TreeItem SelectedItem { get {
                if (Tv1.SelectedNode != null) return (TreeItem)Tv1.SelectedNode.Tag;
                return null;

            }

            set
            {
                 
                if (value == null) { Tv1.SelectedNode = null; }
                else
                {
                     
                      Tv1.SelectedNode = value.Node;
               
                }
            }
        }
        private void Tv1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            
        }

   

        private void RemoveItem(TreeItem itm)
        {
            if (itm.Node.Parent != null)
            {
                TreeItem item = (TreeItem)itm.Node.Parent.Tag;
                item.Items.Remove(itm);    
            }
            else
            {   project.Items.Remove(itm);

            }
           

            Tv1.Nodes.Remove(itm.Node);
            RaiseDataChange();
        }

        private void SetColors(TreeNode node)
        {
            TreeItem itm = (TreeItem)node.Tag;
            if (itm.Items.Count > 0 || itm.Type==ItemType.Folder)
            {
                //node.BackColor = TitleBC;
                node.SelectedImageIndex = 0;
                node.ImageIndex = 0;
                node.NodeFont = new Font(this.Font.FontFamily, 7);
                
                itm.Type = ItemType.Folder;
            }
            else
            {
                //node.BackColor = Color.White;
                node.SelectedImageIndex = 1;
                node.ImageIndex = 1;
                node.NodeFont = new Font(this.Font.FontFamily, 10,FontStyle.Bold );
                itm.Type = ItemType.Item;
            }
        }

        public void LoadTree()
        {
            foreach(TreeItem itm in project.Items)
            {
                TreeNode node = new TreeNode();
                node.Text = itm.Text;
                node.Tag = itm;
                itm.Node = node;
                SetColors(node);
                Tv1.Nodes.Add(node);
                LoadTree(itm);
                if (itm.Expanded) node.Expand();
            }

        }

        private void LoadTree(TreeItem item)
        {
            foreach(TreeItem itm in item.Items)
            {
                TreeNode node = new TreeNode();
                node.Text = itm.Text;
                node.Tag = itm;
                itm.Node = node;
                SetColors(node);
                item.Node.Nodes.Add(node);
                LoadTree(itm);
                if (itm.Expanded) node.Expand();
            }
        }

        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
                
        }

        private void Tv1_MouseDown(object sender, MouseEventArgs e)
        {
            TreeNode node = Tv1.HitTest(e.X, e.Y).Node;
            if (node == null) SelectedItem = null;

            RefreshStrip(node);
            if (node != null)
            {
                
                SelectedItem = (TreeItem)node.Tag;
            }

                if (e.Button == MouseButtons.Right)
                {
                   
                    strip.Show(Cursor.Position);

                }
            
        }

        private void RefreshStrip(TreeNode byNode=null)
        {
             
            addItemToolStripMenuItem.Visible = byNode != null;
            editToolStripMenuItem.Visible = byNode != null;
            deleteToolStripMenuItem.Visible = byNode != null;
            browseToolStripMenuItem.Visible = byNode != null;
            toolStripMenuItem1.Visible = byNode != null;

        }
        private void Tv1_MouseMove(object sender, MouseEventArgs e)
        {
            TreeNode node = Tv1.HitTest(e.X, e.Y).Node;
            if (node != null)
            {
                if (e.Button==MouseButtons.Left)
                {
                    if (node.IsSelected)
                        Tv1.DoDragDrop(node.Text, DragDropEffects.Copy);
                }

            }
        }

        private void Tv1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            TreeItem itm = (TreeItem)e.Node.Tag;
            itm.Expanded = true;

        }

        private void Tv1_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            TreeItem itm = (TreeItem)e.Node.Tag;
            itm.Expanded = false;
        }

        private void Tv1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (SelectedItem != null)
            {
                if (SelectedItem.Node.Nodes.Count == 0)
                {
                    if (settings.SendOnDblClick) CopyPressed(this,

                          new CopyPressEvent { Data = SelectedItem.Text });
                }
            }
        }

        private void Tv1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(System.String)))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void Tv1_DragDrop(object sender, DragEventArgs e)
        {
            
            if (e.Data.GetDataPresent(typeof(System.String)))
            {
                string s = e.Data.GetData(typeof(System.String)).ToString();
                TreeNode DestNode = Tv1.HitTest(Tv1.PointToClient(new Point(e.X,e.Y)) ).Node;
                if (DestNode == null) return;

                TreeItem DestItemParent = null;
                TreeItem DestItem = (TreeItem)DestNode.Tag;
                if (DestItem.Node.Parent != null) DestItemParent = (TreeItem)DestItem.Node.Parent.Tag;

                if (DestNode != null)
                {

                    if (SelectedItem != null)
                    {
                        TreeItem tempItem = SelectedItem;
                        //drag drop postojeceg itema
                        if (SelectedItem.Node.Text == s)
                        {
                            if (SelectedItem.Equals(DestItem)) return;
                            if (DestItem.Type == ItemType.Folder)
                            {
                                RemoveItem(SelectedItem);
                                DestNode.Nodes.Add(tempItem.Node);
                                DestItem.Items.Add(tempItem);
                            }
                            else
                            {
                                if (DestItemParent != null)
                                {
                                    RemoveItem(SelectedItem);
                                    int index = DestNode.Index;
                                    DestItemParent.Node.Nodes.Insert(index, tempItem.Node);
                                    DestItemParent.Items.Insert(index, tempItem);
                                }
                                else
                                {


                                }
                            }
                            SelectedItem = tempItem;
                        }
                        else
                        {
                            //drop iz vana na item
                                AddItem(s, DestItem);
                        }

                        if (SelectedItem.Node.Parent != null)
                            SetColors(SelectedItem.Node.Parent);
                    }
                    
                    else
                    {
                    //drop iz vana na item
                            AddItem(s, DestItem);
                      
                    }

                    SetColors(DestItem.Node);
                    
                }
            }

        }

        private void btnNewItem_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }
        private void AddNewItem()
        {
            AddNewItem(false, ItemType.Item, "");
        }
        private void AddNewItem(bool edit, ItemType type ,string defText="")
        {
            bool tm = form1.TopMost;
            form1.TopMost = false;
            frmNewItem frm = new frmNewItem();
            frm.text.Text = defText;
            if (edit)
            {
                frm.Text = "Edit";
                if (frm.ShowDialog() == DialogResult.OK)
                {

                    SelectedItem.Text = frm.text.Text;
                    SelectedItem.Name = frm.text.Text;
                    SelectedItem.Node.Text = frm.text.Text;

                }
            }
            else
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    
                    AddItem(frm.text.Text,SelectedItem,type);
                }
            }
            form1.TopMost = tm;

        }

        private void addFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewItem(false, ItemType.Folder, "");

        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedItem != null)
                AddNewItem(true, SelectedItem.Type, SelectedItem.Text);
        }

        private void browseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string str = SelectedItem.Text;
            if (!str.ToLower().StartsWith("http")) str = "http://" + str;

            System.Diagnostics.Process.Start(str);
        }

        private void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(SelectedItem.Text);

        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(Clipboard.ContainsText())
                
            AddItem(Clipboard.GetText(), SelectedItem, ItemType.Item);

        }
    }

    public class CopyPressEvent : EventArgs
    {
        public string Data { get; set; }
        public delegate void delDataChange(object sender, EventArgs e);
    }
}
